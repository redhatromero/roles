Module to set sshd for systems

Example:
To set sshd with defaults
ansible-playbook playbooks/finish_script.yml -v -e "{'setup':true}" -i inventory/test/hosts -l vapp-testvm-jgerry-01.ogc01.gdot.ad.local --tags "sshd"

To set sshd without defaults
ansible-playbook playbooks/finish_script.yml -v -e "{'setup':false}" -i inventory/test/hosts -l vapp-testvm-jgerry-01.ogc01.gdot.ad.local --tags "sshd"

In task vars:
#over ride if needed
setup: False
gdot_ssh_allow_groups_extra: (when defined)


