Module to set sudoers for systems

Example:
To set sudoers with defaults
ansible-playbook playbooks/common.yml -v -i inventory/test/hosts -l vapp-testvm-jgerry-01.ogc01.gdot.ad.local --tags "sudoers"

In task vars:
#over ride if needed set in defaults
sudoers_directory: "/etc"
sudoers_visudo_path: "/usr/sbin"
gdot_sudoers_main_template: sudoers.j2
gdot_sudoers_syslog_facility: authpriv
sudoers_aggregate_dir: "/tmp/ansible_sudoers_aggregate/{{ inventory_hostname }}"

