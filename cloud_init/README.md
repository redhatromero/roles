Module to set cloudinit for systems

Example:
To set cloudinit in a prod system
ansible-playbook playbooks/finish_script.yml -v -e "{'prod':true}" -i inventory/test/hosts -l vapp-testvm-jgerry-01.ogc01.gdot.ad.local --tags "cloud_init"

To set chronyd in a nonprod system
ansible-playbook playbooks/finish_script.yml -v -e "{'prod':false}" -i inventory/test/hosts -l vapp-testvm-jgerry-01.ogc01.gdot.ad.local --tags "cloud_init"

Local Vars:
#over ride if needed
foremanserver: "vapp-foreman-01.ogc01.gdot.ad.local"
#finish - tells forman server we are done with the install
finsh: False

Global Vars:
#Lists of servers for each environment
ldapserversprod
ldapserversnonprod

