#!/bin/bash

TYPE=$1
NAME=$2
STATE=$3

COUNT=`ps aux | grep -c INSTANCE`

while [ $COUNT -gt 3 ]; do
	echo "one at a time, not $COUNT please.."
	COUNT=`ps aux | grep -c INSTANCE`
done

case $STATE in
	"MASTER")
                echo "I am captain now"
                STATUS=`/sbin/drbdadm status gitlab | grep "gitlab role" | cut -d: -f2`
                echo $STATUS
                while [ $STATUS != "Primary" ]; do
                        /sbin/drbdadm primary gitlab
                        echo "Sleeping 5"
                        sleep 5
                        STATUS=`/sbin/drbdadm status gitlab | grep "gitlab role" | cut -d: -f2`
                done
                echo "I have the con.  Starting gitlab"
                mount /var/opt/gitlab
                /bin/gitlab-ctl start
                ;;
        *)
          	echo "Someone else took over the ship"
                /bin/gitlab-ctl stop
		OPENFILES=`lsof | grep -c /var/opt/gitlab`
                while [ $OPENFILES -ne 0 ]; do
                        echo "Cant unmount with $OPENFILES open files, sleeping"
                        sleep 5
                        /bin/umount /var/opt/gitlab
			OPENFILES=`lsof | grep -c /var/opt/gitlab`
                done

                /bin/umount -f /var/opt/gitlab
                STATUS=`/sbin/drbdadm status gitlab | grep "gitlab role" | cut -d: -f2`
                echo $STATUS
                while [ $STATUS != "Secondary" ]; do
                        /sbin/drbdadm secondary gitlab
                        echo "Sleeping 5"
                        sleep 5
                        STATUS=`/sbin/drbdadm status gitlab | grep "gitlab role" | cut -d: -f2`
                done
                ;;
esac
