#!/bin/bash

TYPE=$1
NAME=$2
STATE=$3

COUNT=`ps aux | grep -c INSTANCE`

while [ $COUNT -gt 3 ]; do
	echo "one at a time, not $COUNT please.."
	COUNT=`ps aux | grep -c INSTANCE`
done

case $STATE in
	"MASTER")
						echo "I am captain now."
						;;
        *)
          	echo "Someone else took over the ship?"
						;;
esac
