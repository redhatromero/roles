# Self-Signed Cert

Example variable structure to use for this role:
```
ssc:
  privatekey:
    path: "/etc/ssl/private/{{ ansible_hostname }}.pem"
    bits: 2048
  csr:
    path: "/etc/ssl/csr/{{ ansible_hostname }}.csr"
    country: 'US'
    state: 'Georgia'
    locality: 'Atlanta'
    organization: 'Georgia Department of Transportation'
    org_unit: 'Information Technology'
    common_name: "{{ ansible_hostname }}"
    email: 'unixadmins@dot.ga.gov'
    san:
      - "DNS:{{ ansible_fqdn }}"
      - "IP:{{ ansible_default_ipv4.address }}"
    cert:
      path: "/etc/ssl/certs/{{ ansible_hostname }}.crt"
    dhparam:
      path: "/etc/ssl/dhparam/{{ ansible_hostname }}.pem"
      size: 2048
```
