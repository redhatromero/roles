Module to set sssd for systems

Includes Roles: sshd, sudoers, update_packages

Example:
To set sssd in a prod system
ansible-playbook playbooks/finish_script.yml -v -e "{'prod':true}" -i inventory/test/hosts -l vapp-testvm-jgerry-01.ogc01.gdot.ad.local --tags "sssd"

To set chronyd in a nonprod system
ansible-playbook playbooks/finish_script.yml -v -e "{'prod':false}" -i inventory/test/hosts -l vapp-testvm-jgerry-01.ogc01.gdot.ad.local --tags "sssd"

Local Vars:
#over ride if needed
prod: False

Global Vars:
#Lists of servers for each environment
ldapserversprod 
ldapserversnonprod
