Module to register and unregister systems in RSAT

To Register:
ansible-playbook playbooks/subscription.yml -v -e "{'registersystem':true}" -i inventory/test/hosts -l vapp-testvm-jgerry-01.ogc01.gdot.ad.local

To Unregister:
ansible-playbook playbooks/subscription.yml -v -e "{'registersystem':false}" -i inventory/test/hosts -l vapp-testvm-jgerry-01.ogc01.gdot.ad.local

Default:
#Allows for system registration
registersystem: True
#Flag to edit hosts file in non prod environments
prod: False
