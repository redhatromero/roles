This role modifies hosts files:

- It adds default localhost like default
- It adds any group_var or localhost var as defined for any further hosts file entries as needed
- Went with a very simplistic line by line approach vs joins or breaking out ip and name list

Var file format Example:

host_file_entries:
    - serverentry: 143.100.90.99 gdot-go-rhsat01.gdot.ad.local gdot-go-rhsat01

