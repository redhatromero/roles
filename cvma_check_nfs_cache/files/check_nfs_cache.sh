#!/bin/sh
#
# Author:      Michael Morris
# Description: Compare files' inodes between all NFS mounts on all CommVault
#              Media Agents.  Report differences via email.
################################################################################
# USER-CONFIGUABLE VARIABLES #
##############################
parjobs=4
emlto="unixadmins@dot.ga.gov"
emlsub="ALERT: Suspected NFS-Caching Issue on Commvault Media Agents"
emlfrom="noreply@dot.ga.gov"

share_list="
prdENTDB
prdENTDB01
prdCIFS101
prdEDWDB
prdCIFS202
prdCIFS201
prdCIFS102
prdLinuxOS
prdLinuxOSIncr
prdEDWOS
prdENTOS
"

# Place in array so we can retrieve first element for comparison.
host_list=(
   gdot-go-cvma05
   gdot-go-cvma06
   gdot-go-cvma07
   gdot-go-cvma10
)


#########################
# CONVENIENCE VARIABLES #
################################################################################
mydir=$(dirname ${0})
lckfile=${mydir}/cknfs.lck
parfile=${mydir}/cknfs.par
logfile=${mydir}/cknfs.log
outfile=${mydir}/cknfs.out
emlfile=${mydir}/cknfs.eml
htmfile=${mydir}/cknfs.htm


#############
# FUNCTIONS #
################################################################################
# Since we email a couple of times, set it in a function
email() {
   #mailx -s ${emlsub} -r ${emlfrom} ${emlto}
   sendmail ${emlto}
}


# Function to check each side of the sdiff output and confirm we're comparing
# like files
match_sides() {
   # Backup $IFS variable and set it to @ for while-loop parsing
   IFS="${IFS_BAK}"
   IFS='@'

   # Open file handle with diffout as input for while-loop
   exec 3<&0
   exec 0<<-EOF
${diffout}
EOF

   # Loop through each line and confirm they are matching file names
   while read left_file t1 t2 right_file t3 ;do
      if [[ "${left_file}" = "${right_file}" ]] ;then
         echo "${left_file}@${t1}@${t2}@${right_file}@${t3}"
      fi
   done

   # Close file descriptor and set IFS back to original value
   exec 3<&-
   IFS="${IFS_BAK}"
}


# Function to make output prettier by tabular formatting (columns)
tabular_output() {
   # Backup $IFS variable and set it to @ for while-loop parsing
   IFS_BAK="${IFS}"
   IFS='@'

   # Open file handle with diffout as input for while-loop
   exec 3<&0
   exec 0<<-EOF
${diffout}
EOF

   # Loop through each line and calibrate for largest of each field
   lf=0 ;li=0 ;dl=0 ;rf=0 ;ri=0
   while read left_file left_inode delimeter right_file right_inode ;do
      if [ ${#left_file}   -gt ${lf} ] ;then lf=${#left_file}   ;fi
      if [ ${#left_inode}  -gt ${li} ] ;then li=${#left_inode}  ;fi
      if [ ${#delimeter}   -gt ${dl} ] ;then dl=${#delimeter}   ;fi
      if [ ${#right_file}  -gt ${rf} ] ;then rf=${#right_file}  ;fi
      if [ ${#right_inode} -gt ${ri} ] ;then ri=${#right_inode} ;fi
   done

   # Close file descriptor
   exec 3<&-

   # Open file handle with diffout as input for while-loop
   exec 3<&0
   exec 0<<-EOF
${diffout}
EOF

   # Loop through each line and print with padding
   while read left_file left_inode delimeter right_file right_inode ;do
      printf "%-${lf}s %-${li}s %${dl}s %-${rf}s %-${ri}s\n" \
             "${left_file}" "${left_inode}" "${delimeter}" \
             "${right_file}" "${right_inode}"
   done

   # Close file descriptor and set IFS back to original value
   exec 3<&-
   IFS="${IFS_BAK}"
}


#############
# EXECUTION #
################################################################################
# Check for a lockfile and warn appropriately if one exists.  If it doesn't
# exist, create one.
if [[ -f ${lckfile} ]] ;then
   echo -e "Lockfile exists at $(hostname):$(readlink -f ${lckfile}).  Not \
running checks.\n\nPlease confirm no current jobs are stalled and remove the \
lockfile to allow future jobs to continue." | email
   exit 1
fi

echo $$ >${lckfile}


# Clear all output from previous executions.
cp /dev/null ${parfile}
cp /dev/null ${outfile}
cp /dev/null ${emlfile}
echo -e "Seq\tHost\tStarttime\tJobRuntime\tSend\tReceive\tExitval\tSignal\tCommand" >${logfile}


# Create a job file for the parallel command to execute.
# This job file will basically run a find command through
# all StoreOnce mounts on all Media Agent hosts and print
# out the file name and inode number.
for share in ${share_list} ;do
   parfile=${mydir}/${share}.par
   cp /dev/null ${parfile}
   for host in ${host_list[@]} ;do
      echo "ssh ${host} \"find /so/${share} -printf \\\"%p %i\\n\\\"\" \
2>/dev/null | sort -d >${mydir}/${share}_${host}" >>${parfile}
   done
done


# Execute the parallel command from the job file created above.
for share in ${share_list} ;do
   parfile=${mydir}/${share}.par
   outfile=${mydir}/${share}.out
   ( time /opt/parallel/bin/parallel -j ${parjobs} --joblog +${logfile} -a ${parfile} ) >${outfile} 2>&1
done


# Compile a report of differences by comparing all shares of all servers to
# gdot-go-cvma05.
for share in ${share_list} ;do
   for host in ${host_list[@]} ;do
      if [[ "${host}" != "${host_list[0]}" ]] ;then
         diffout=$(sdiff -t -w 1000 ${mydir}/${share}_${host_list[0]} ${mydir}/${share}_${host} |\
                 grep '[|]' | sed 's/ \+/@/g')
         if [[ "${diffout}" != '' ]] ;then
            diffout=$(match_sides)
            diffout=$(tabular_output)
            echo -e "\n${host_list[0]} vs. ${host}:" >>${emlfile}
            echo "${diffout}" >>${emlfile}
         fi
      fi
   done
done


# Email the report if exists.
if [[ -s ${emlfile} ]] ;then
   cat <<-EOF >${htmfile}
From: ${emlfrom}
To: ${emlto}
Subject: ${emlsub}
MIME-Version: 1.0
Content-Type: text/html; charset=us-ascii
Content-Disposition: inline
Listed below are files that have the same name between two systems but \
have differing inodes.  This information should be used as a lead for \
investigation into an NFS cache coherency issue between the two \
systems specified.
<html><body><pre><font face=courier size=2>
$(cat ${emlfile})
</font></pre></body></html>
EOF

   cat ${htmfile} | email
fi


# Remove the lockfile indicating we are ready for a new run.
rm ${lckfile}
