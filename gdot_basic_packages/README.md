Module to install basic troubleshooting package in systems

Current list:
vim
nano
nmap
tcpdump
strace
lsof
net-tools

Example:
To install these packages
ansible-playbook playbooks/finish_script.yml -v -e "{'setup':true}" -i inventory/test/hosts -l vapp-testvm-jgerry-01.ogc01.gdot.ad.local --tags "gdot_basic_packages"

To uninstall these packages
ansible-playbook playbooks/finish_script.yml -v -e "{'setup':false}" -i inventory/test/hosts -l vapp-testvm-jgerry-01.ogc01.gdot.ad.local --tags "gdot_basic_packages"

