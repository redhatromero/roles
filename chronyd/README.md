Module to set chronyd for systems

Exmample:
To set chronyd in a nonprod system
ansible-playbook playbooks/finish_script.yml -v -e "{'prod':true}" -i inventory/test/hosts -l vapp-testvm-jgerry-01.ogc01.gdot.ad.local --tags "chronyd"

To set chronyd in a prod system
ansible-playbook playbooks/finish_script.yml -v -e "{'prod':false}" -i inventory/test/hosts -l vapp-testvm-jgerry-01.ogc01.gdot.ad.local --tags "chronyd"

Defaults:
#Allows for system registration
registersystem: True
#Flag to edit hosts file in non prod environments
prod: False
