#!/bin/bash -x
#############################################################################
# Purpose  : Stop Oracle Database and related services
# Author   : Suraj Reddy
##############################################################################

GetPathAndCmd()
{
MyCMD=$0
cd ${MyCMD%/*}
MyPath=`pwd`
MyName=${MyCMD##*/}
MyNoExtName=${MyName%.*}
}

####
GetPathAndCmd
host_name=`hostname -s`
vdate=`date "+%Y%m%d_%H%M"`
logdir=/backup_exp/temp
logfile=${logdir}/${host_name}.log
svrfile=${MyPath}/oracle_db_shutdown.cfg
curr_hostname=${host_name:0:7}
status_cmd='status resource -t'
stop_cmd1='stop has'
stop_cmd2='stop crs'
ctl_cmd=`grep "^${host_name}:" ${svrfile} | grep -v "^#" | cut -d: -f2 -s` 
stop_flag=`grep "^${host_name}:" ${svrfile} | grep -v "^#" | cut -d: -f3 -s`

if [ -d ${logdir} ]; then
echo
else
mkdir -p ${logdir}
fi 

echo " # " > ${logfile}
echo "INFO : $vdate :  ----------------------------------" >> ${logfile}
echo "INFO : $vdate :  Process SQL script	: ${0}" >> ${logfile}
echo "INFO : $vdate :  Log file				: ${logfile}" >> ${logfile}
echo "INFO : $vdate : -----------------------------------" >> ${logfile}
echo "                               " >> ${logfile}
echo "INFO : $vdate : Script started."  >> ${logfile}
if [ ${stop_flag} == "Y" ]; then
if [ ${curr_hostname} == 'gdot-de' ]  || [ ${curr_hostname} == 'gdot-tr' ] ; then
echo "INFO : $vdate : Resource on Server $host_name are being stopped"   >> ${logfile}
sudo $ctl_cmd ${status_cmd} >> ${logfile}
sudo $ctl_cmd ${stop_cmd1}	>> ${logfile}
elif [ ${curr_hostname} == 'gdot-go' ]  || [ ${curr_hostname} == 'gdot-bc' ] || [ ${curr_hostname} == 'gdot-ts' ] ; then
echo "INFO : $vdate : Resource on Server $host_name are being stopped" >> ${logfile}
sudo $ctl_cmd ${status_cmd}	>> ${logfile}
sudo $ctl_cmd ${stop_cmd2}	>> ${logfile}
else
echo "INFO : $vdate : Server $host_name does not belong to Enterprise Database Team"  >> ${logfile}
fi
echo "INFO : $vdate : Script finished."  >> ${logfile}
else
echo "INFO : $vdate : Script finished w/o Shutdown since stop flag was set to $stop_flag."  >> ${logfile}
fi

