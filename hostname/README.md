Simple Hostname change via ansible.

uses one variable which sets VM hostname

#Not yet implemented
-Will add vmware rename capability later
-May add foreman/satellite rename later

Example:
ansible-playbook playbooks/finish_script.yml -v -e "systemhostname:supertest" -i inventory/test/hosts -l vapp-testvm-jgerry-01.ogc01.gdot.ad.local --tags "hostname"

Vars:
systemhostname
