Example host_vars configuration:

---
nfs_exports:
  - "/export/atms_common 172.20.203.0/24(rw,sync,no_root_squash)"
