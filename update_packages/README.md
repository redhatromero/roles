Module to upgrade basic packages with yum

Example:
To upgrade package
ansible-playbook playbooks/finish_script.yml -v -e "{'setup':true}" -i inventory/test/hosts -l vapp-testvm-jgerry-01.ogc01.gdot.ad.local --tags "update_packages"

