# Ansible Controller
On the face of it, this role is a bit of a "chicken and egg" scenario.  In order
to deploy the role, you must have Ansible installed.  But if you already have
Ansible installed, what's the point of the role?  Well, it can be used to keep a
consistent environment for everybody utilizing Ansible to maintain a common set
of infrastructure.  Also, it can be used to create an Ansible Controller
deployment for other infrastructure components like Ansible runners in Foreman.

Ansible development is fairly rapid and with more than a handful of people using
it to manage a large number of systems, version issues will arise in the
organization.  Tasks might not work as intended if a person uses a lower or
higher version of Ansible than the original task was written against.

Ultimately, when used, this role should place every person and automated-user of
Ansible on the same relative Ansible platform (Python, Ansible, Ansible
dependencies) version while allowing the user Ansible-configuration freedom.


## Quickstart
This will get a system/user installed with Ansible in as few steps as possible.
See the _Full_ example below in the _User-Overrides_ section for more variable
overrides.

1. **Add** the following to `inventory/ENV/host_vars/FQDN/ansible_ctrl.yml`
    ```
    ---
    ansible_ctrl_users:
      - name: YOUR_USER_ID
    ```

2. **Run** the Ansible playbook
    ```
    $ ansible-playbook ./playbooks/ansible_ctrl.yml -i ./inventory/ENV/hosts -l FQDN
    ```


## Role Overview
```
ansible_ctrl
|-- defaults
|   `-- main.yml     <-- Default variables to be, mostly, overridden by
|                        host_vars.
|-- files
|   |-- krb5.conf    <-- Default /etc/krb.conf including both gdot.ad.local
|   |                    (Production) and tgdot.tst.local (Non-Prod) Windows
|   |                    domains.
|   |-- passwd.gdot  <-- The GDOT Ansible vault identity password.  It is
|   |                    ansible-vault encrypted for security in external remote
|   |                    git repos on the network.
|   `-- req_py3_ansible.txt  <-- Default requirements file; updated when a new
|                                one is qualified.
|-- tasks
|   |-- RedHat.yml   <-- Red Hat specific tasks.  Mainly enabling repo and
|   |                    installation of system-level packages and dependencies.
|   `-- main.yml     <-- Main tasks driven, mainly, around user environment.
|                        Including user assessment, virtual environment
|                        creation, and user config file management.
|-- templates
|   `-- ansible.cfg  <-- A base template for users' Ansible configuration for
|                        use in GDOT's systems.  This file uses variables for
|                        'forks' and 'roles_path'.
`-- vars
    `-- RedHat.yml  <--- Red Hat specific variables.  Mainly, a package list
                         specific to Red Hat.
```


## Variables
### Defaults
These are variables contained in the `defaults/main.yml` file that can be mostly
overridden by host variables in inventory per system.  And, in most cases by the
`ansible_ctrl_users` host variable specified in the inventory `host_vars`.

- `ansible_ctrl_pip_req`: The "requirements.txt" for qualified Ansible build.
                          Generally, this variable is not overridden as it is
                          the definition of what the qualified virtual
                          environment is.
- `ansible_ctrl_venv_dir`: Directory to install the base virtual environment
                           into made from the PIP requirements file.
- `ansible_ctrl_vault_id_gdot`: This is the vault password file to be used for
                                the GDOT encrypted vault files.
- `ansible_ctrl_roles`: Default roles path.  Can be overridden via
                        `ansible_ctrl_users` list variable in host_vars.
- `ansible_ctrl_cfg_template`: Default Ansible config template to use.  Can be
                               overridden by `ansible_ctrl_users` so that they
                               can store a version of their preferred template
                               in the templates directory of the role.
- `ansible_ctrl_src_venv`: Whether to append the sourcing code to the user's
                           .bash_profile so by default they have this virtual
                           environment loaded on every login.  Default is True
                           to assist with completeness but can be overridden via
                           `ansible_ctrl_users` list variable in host_vars.
- `ansible_ctrl_cfg_overwrite`: Allows for Ansible configuration overwrite or to
                                only do it on the non-existance of the file.
                                Defaults to False so the user can maintain their
                                own configuration file if they want.
- `ansible_ctrl_forks`: This should be overridden by `host_vars` based on the
                        processors in the host.  Ansible defaults this value to
                        5 out of the box.


### User-Overrides
We can override these default variables for the user by specifying them in
`host_vars/FQDN/ansible_ctrl.yml`.

Minimal:
```
---
ansible_ctrl_users:
  - name: C0005112
```

Full:
```
---
ansible_ctrl_forks: 8
ansible_ctrl_users:
  - name: C0005112
    roles: ~/ansible_repos/gdot/roles:~/ansible_repos/vendorX/roles
    cfg_template: ansible.cfg-C0005112
    cfg_overwrite: yes
    venv_dir: venv_py3_ansible
    src_venv: no
```


## Tasks
The associated _playbook_ has a failsafe condition to skip the role if
`ansible_ctrl_users` is undefined.  The _role_ also has a failsafe to fail if
`ansible_ctrl_users` is undefined.

Since most of the tasks to create an Ansible controller environment are really
architecture independent, the first set of tasks lay out a foundation on the
system using system-specific package management and any other configurations
necessary.  These are included from an `ansible_distribution` variable gleamed
from fact-gathering modules.

The _Assess Users_ section focuses on deriving information from the users
specified in the inventory's `ansible_ctrl_users` list variable.  It uses a
method of consolidating registered results from previous tasks and finally
stripping them down into a new, simple, `ansible_ctrl_users` list variable to be
referenced for the rest of the tasks in the role.

Since we use a list for flexibility of specifying, potentially, more than one
user on a host, most all other tasks in the role are formulated around a loop
of that list variable.

