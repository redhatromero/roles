Module to set virtual_disk for systems

Limited usage at the moment and makes some assumption

Example:
To set virtual_disk in a system with /dev/sdb
ansible-playbook playbooks/finish_script.yml -v -i inventory/test/hosts -l vapp-testvm-jgerry-01.ogc01.gdot.ad.local --tags "virtual_disk"

Note it dynamically grabs the blkid of the device, creates and mounts an XFS disk. It won't overwrite data if its mounted
